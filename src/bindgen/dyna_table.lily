import introspect
import (gen_output,
        gen_pkg_name) "bindgen/common"
import (ClassSym,
        DefineSym,
        EnumSym,
        MethodSym,
        ModuleSym,
        ParameterSym,
        PropertySym,
        SymScope,
        VariantSym,
        VarSym) "shared/symbols"

#[
Dyna stage
    Generate the dynaload table.

    Classes and enums add themselves to dyna_class_names, which goes at the top
    of the dynaload table. The interpreter creates a table of ids that is filled
    in with the names of classes as they load. That table (the cid table) is
    used to grab class ids by the module's foreign functions. The id table is
    necessary because all symbols are loaded only on demand and their ids are
    given out when they're loaded.

    Variants are not included in the names because variant ids are always
    relative to their enum. If, for example, an enum has id N, the first variant
    has id N + 1, the second N + 2, and so on.
]#

var dyna_class_names: List[String] = []

define generics_to_s(generic_list: List[introspect.TypeEntry]): String
{
    var generics = generic_list.map(|m| m.class_name()).join(",")

    if generics: {
        generics = "[" ++ generics ++ "]"
    }

    return generics
}

define parent_to_s(boxed_name: Option[String]): String
{
    var name = boxed_name.unwrap_or("")

    if name: {
        name = "< " ++ name
    }

    return name
}

define oct(num: Integer): String
{
    var result = ""

    while num: {
        var digit = num % 8
        result = digit.to_s() ++ result
        num = num / 8
    }

    return "0" ++ result
}

define function_data_to_s(source_type: introspect.TypeEntry,
                          parameters: List[ParameterSym],
                          result_type: introspect.TypeEntry): String
{
    var out: List[String] = []
    var end = parameters.size() - 1
    var is_vararg = source_type.is_vararg_function()
    var args = ""

    for i in 0...end: {
        var p = parameters[i]
        var out_str = p.type
                       .as_string()
                       .replace(" ", "")

        if is_vararg && i == end: {
            var slice_start = 5
            var star = ""

            # Just in case the vararg is optional (ex: "*List[x]")
            if out_str.starts_with("*"): {
                slice_start += 1
                star = "*"
            }

            out_str = star ++ out_str.slice(slice_start, -1) ++ "..."
        }

        if p.keyword: {
            out_str = ":" ++ p.keyword ++ " " ++ out_str
        }

        out.push(out_str)
    }

    # This check is necessary because empty () isn't allowed.
    if parameters.size(): {
        args = "(" ++ out.join(",") ++ ")"
    }

    var result = result_type.as_string()
                            .replace(" ", "")

    if result: {
        # Unit is implied and cannot be explicitly stated.
        if result == "Unit": {
            result = ""
        else:
            # This prevents the result from being seen as a keyarg.
            result = ": " ++ result
        }
    }

    result = args ++ result
    return result
}

define var_proto_to_s(t: introspect.TypeEntry): String
{
    return t.as_string()
            .replace(" ", "")
}

define on_dyna_definitions(d: DefineSym)
{
    var dyna_letter = "F"

    var type = function_data_to_s(d.type, d.parameters, d.result_type)
    var generics = generics_to_s(d.generics)

    gen_output.push(
    "    ,\"" ++ dyna_letter ++ "\\0"
              ++ d.name ++ "\\0"
              ++ generics
              ++ type
              ++ "\"")
}

define on_dyna_methods(m: MethodSym)
{
    var dyna_letter = "m"
    var type = function_data_to_s(m.type, m.parameters, m.result_type)
    var generics = generics_to_s(m.generics)

    gen_output.push(
    "    ,\"" ++ dyna_letter ++ "\\0"
              ++ m.name ++ "\\0"
              ++ generics
              ++ type
              ++ "\"")
}

define on_dyna_vars(v: VarSym)
{
    var dyna_letter = "R"
    var type = var_proto_to_s(v.type)

    gen_output.push(
    "    ,\"" ++ dyna_letter ++ "\\0"
              ++ v.name ++ "\\0"
              ++ type
              ++ "\"")
}

define on_dyna_variants(v: VariantSym)
{
    var dyna_letter = "V"
    var args = v.parameters
    var arg_strings: List[String] = []
    var type_str = ""

    for i in 0...args.size() - 1: {
        var arg = args[i]
        var arg_str = arg.type.as_string()
                              .replace(" ", "")

        if arg.keyword: {
            arg_str = ":" ++ arg.keyword ++ " " ++ arg_str
        }

        arg_strings.push(arg_str)
    }

    if args.size(): {
        type_str = "(" ++ arg_strings.join(",") ++ ")"
    }

    gen_output.push(
    "    ,\"" ++ dyna_letter ++ "\\0"
              ++ v.name ++ "\\0"
              ++ type_str
              ++ "\"")
}

define on_dyna_enums(e: EnumSym)
{
    dyna_class_names.push(e.name)

    var dyna_letter = "E"
    var generics = generics_to_s(e.generics)
    var total_size = e.methods.size()

    if e.is_scoped: {
        total_size += e.variants.size()
    }

    gen_output.push(
    "    ,\"" ++ dyna_letter ++ "\\" ++ oct(total_size)
              ++ e.name ++ "\\0"
              ++ generics
              ++ "\"")

    e.methods.each(on_dyna_methods)
    e.variants.each(on_dyna_variants)
}

define on_dyna_foreign_class(c: ClassSym)
{
    dyna_class_names.push(c.name)

    var dyna_letter = "C"
    var generics = generics_to_s(c.generics)
    var total_size = c.methods.size()

    gen_output.push(
    "    ,\"" ++ dyna_letter ++ "\\" ++ oct(total_size)
              ++ c.name ++ "\\0"
              ++ generics
              ++ "\"")

    c.methods.each(on_dyna_methods)
}

define dyna_letter_for_scope(scope: SymScope): String
{
    match scope: {
        case SymScope.Public:
            return "3"
        case SymScope.Protected:
            return "2"
        case SymScope.Private:
            return "1"
    }
}

define on_dyna_properties(p: PropertySym)
{
    var dyna_letter = dyna_letter_for_scope(p.scope)
    var proto = var_proto_to_s(p.type)

    gen_output.push(
    "    ,\"" ++ dyna_letter ++ "\\0"
              ++ p.name ++ "\\0"
              ++ proto
              ++ "\"")
}

define on_dyna_native_class(c: ClassSym)
{
    dyna_class_names.push(c.name)

    var dyna_letter = "N"
    var generics = generics_to_s(c.generics)
    var parent = parent_to_s(c.parent_name)
    var total_size = c.methods.size() + c.properties.size()

    gen_output.push(
    "    ,\"" ++ dyna_letter ++ "\\" ++ oct(total_size)
              ++ c.name ++ "\\0"
              ++ generics
              ++ parent
              ++ "\"")

    c.methods.each(on_dyna_methods)
    c.properties.each(on_dyna_properties)
}

define on_dyna_classes(c: ClassSym)
{
    if c.is_native: {
        on_dyna_native_class(c)
    else:
        on_dyna_foreign_class(c)
    }
}

define start_dyna_table(module: ModuleSym): ModuleSym
{
    gen_output.push("LILY_{0}_EXPORT".format(gen_pkg_name.upper()))
    gen_output.push("const char *lily_" ++ gen_pkg_name ++ "_info_table[] = {")
    gen_output.push("")

    var patch_index = gen_output.size()

    module.classes.each(on_dyna_classes)
    module.enums.each(on_dyna_enums)
    module.definitions.each(on_dyna_definitions)
    module.vars.each(on_dyna_vars)

    gen_output.push("    ,\"Z\"")
    gen_output.push("};")

    if gen_pkg_name == "prelude": {
        dyna_class_names = []
    }

    var dyna_names = dyna_class_names.join("\\0")
    var dyna_size = dyna_class_names.size() |> oct

    dyna_class_names = []
    gen_output[patch_index-1] =
            "    \"\\{0}{1}\\0\"".format(dyna_size, dyna_names)

    return module
}
