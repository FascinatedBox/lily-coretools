lily-coretools
==============

Tools for working with Lily libraries.

To install, run `garden install FascinatedBox/lily-coretools`.

This library currently contains the following tools:

## Bindgen

Bindgen is an essential part of writing a foreign Lily library. It generates the
tables that the interpreter uses to load foreign libraries. It also generates
some useful macros, all of which is written to a bindings file. The binding file
is a plain C header file that can be included. What to generate is determined by
reading the definitions in a Lily manifest file.

Bindgen works by reading a Lily file in manifest mode. Manifest mode is a
special mode that only allows definitions. Writing a manifest file is like
writing a native Lily library, except that the definitions are filled by C
instead of Lily.

Manifest files begin with `import manifest` but they don't actually import a
module named manifest. They start with that line to make sure files don't get
interpreted in the wrong mode.

Let's say there's a library called `animals` that has a `Cat` class and a `Dog`
class. A manifest file might look like this:

```
import manifest

### Demo library with animal examples.
###
### This is a demo showing how to write a manifest file.
library animals

### A class representing a cat.
###
### This class is able to meow and speak.
class Cat
{
    public define meow
    public define speak(what: String)
}

var my_cat: Cat

### A class representing a dog.
###
### This class can bark instead of meow.
class Dog
{
    public define bark
    public define speak(what: String)
}
```

Given the above file, bindgen will generate bindings to discover all of the
symbols listed above. It assumes that the source of the library is in a
directory named `src`, with manifest files being in a directory named
`manifest`.

## Docgen

Docgen is a tool for generating documentation for both foreign and native Lily
libraries. Documentation is written as a series of html files to a directory
that defaults to `doc/`. To show the options that docgen has, run it without
any arguments.

To generate documentation for a foreign library, invoke docgen with the path of
the library's manifest file.

For native libraries, invoke docgen with the path of the library's main file.
Docgen will read the native library with a subinterpreter, but won't execute any
of the library's code.
